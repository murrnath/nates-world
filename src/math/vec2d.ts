class Vec2d 
{
  x: number;
  y: number;

  constructor(x:number, y:number)
  {
    this.x = x;
    this.y = y;
  }

  public static ZERO() 
  {
    return new Vec2d(0, 0);
  }

  public static DISTANCE(one: Vec2d, two: Vec2d) : number
  {
    return Math.sqrt( (one.x - two.x) + (two.y - two.y) );
  }

  public magnitude() : number 
  {
    return Math.sqrt( (Math.pow(this.x, 2)) + (Math.pow(this.y, 2)) );
  }

  public times(val: number) : Vec2d
  {
    this.x *= val;
    this.y *= val;

    return new Vec2d(this.x * val, this.y * val);
  }

  public plus(val: Vec2d) : Vec2d
  {
    return new Vec2d(this.x + val.x, this.y + val.y);
  }

  public minus(val: Vec2d) : Vec2d
  {
    return new Vec2d(this.x - val.x, this.y - val.y);
  }

  public equals(val: Vec2d) : boolean
  {
    return ( this.x === val.x && this.y === val.y);
  }

  public difference(val: Vec2d) : Vec2d
  {
    const x = this.x - val.x;
    const y = this.y - val.y;

    return new Vec2d(x, y);
  }
}

export default Vec2d;
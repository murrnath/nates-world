import 'phaser';
import Vec2d from './vec2d';

class Math 
{
  public static BoxCollide(pos1: Vec2d, size1: Vec2d, pos2: Vec2d, size2: Vec2d, isLeft: boolean) : boolean
  {
    let hit = false;

    //2 boxes
    //i forgot that box 2 is bound from the bottom
    //bind box 1 from the center i think i can do this
    
    //so i actually need to just reverse this if they are left because of the way i set it up
    if(isLeft) {
      if( pos1.x > pos2.x - (size2.x / 2) && pos1.x - size1.x < pos2.x + (size2.x / 2) ) {
        //do the y now
        if( pos1.y + (size1.y / 2) > pos2.y - size2.y && pos1.y - (size1.y / 2) < pos2.y) {
          hit = true;
        }
      }
    }
    else {
      if( pos1.x + size1.x > pos2.x - (size2.x / 2) && pos1.x < pos2.x + (size2.x / 2) ) {
        //do the y now
        if( pos1.y + (size1.y / 2) > pos2.y - size2.y && pos1.y - (size1.y / 2) < pos2.y) {
          //kay this is real hit nah its not
          hit = true;
        }
      }
    }

    return hit;
  }

  public static zCheck(z_pos1: number, z_pos2: number, z_tolerance:number) : boolean
  {
    const diff = z_pos1 - z_pos2;
    return ( (diff <= z_tolerance) && (diff >= -z_tolerance) );
  }
}

export default Math;
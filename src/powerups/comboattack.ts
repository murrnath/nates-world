/***
 * This will really function less like a normal attack a more like an attack manager
 * its going to let me expand the system and really make some cool combos
 */

import 'phaser';
import Attack from './attack';
import NMath from '../math/math';
import DoubleLinkedList from '../global/doublelinkedlist';
import Node from '../global/node';

class ComboAttack
{
  private attackList: DoubleLinkedList;
  private current: Node;
  private comboDelay: number;
  private comboTimer: number;

  constructor(attacks: Array<Attack>, comboDelay: number)
  {
    this.attackList = new DoubleLinkedList();
    this.comboDelay = comboDelay;

    for ( let key in attacks ) {
      const attack = attacks[key];

      this.attackList.addNode(new Node(attack));
    }

    this.current = this.attackList.head;
  }

  public attack()
  {
    //we just need to make logic to switch through the attacks in the attack list in order
    //im going to write a double linked list for this because i think its stupid if i don't
    this.current.data.attack();
    this.comboTimer = this.comboDelay + this.current.data.getLockTime();

    //switch the attack out now
    if(this.current.next) {
      this.current = this.current.next;
    } else {
      this.reset();
    }
  }

  public tick(delta: number)
  {
    if (this.comboTimer > 0) {
      this.comboTimer -= delta;
    } else {
      this.reset();
    }
  }

  public getAnim()
  {
    return this.current.data.getAnim();
  }

  private reset() 
  {
    this.current = this.attackList.head;
  }
}

export default ComboAttack;
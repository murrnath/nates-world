import 'phaser';
import Vec2d from '../math/vec2d';
import ActionSprite from '../sprites/actionsprite';

class Attack 
{
  protected name: string;
  protected anim: string;
  protected hitBox: Vec2d;
  protected zTol: number;
  protected lockTime: number;
  protected damage: number;
  protected owner: ActionSprite;
  protected offset: Vec2d;

  constructor(name: string, anim: string, hitBox: Vec2d, offset: Vec2d, 
    zTol: number, lockTime: number, damage: number, owner: ActionSprite)
  {
    this.name = name;
    this.anim = anim;
    this.hitBox = hitBox;
    this.zTol = zTol;
    this.lockTime = lockTime;
    this.damage = damage;
    this.owner = owner;
    this.offset = offset;
  }

  public attack()
  {
    //override this lets go boys
  }

  protected notMe(sprite: ActionSprite)
  {
    return sprite.getName() !== this.owner.getName();
  }

  // **** Getters and Setters ****
  public getLockTime() : number { return this.lockTime; }
  public getDamage() : number { return this.damage; }
  public getOffset() : Vec2d { return this.offset; }
  public getOwner() : ActionSprite { return this.owner; }
  public getHitBox() : Vec2d { return this.hitBox; }
  public getName() : string { return this.name; }
  public getAnim() : string { return this.anim; }
}

export default Attack;
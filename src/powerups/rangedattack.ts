/** 
 * Ranged attacks are special they add a projectile into the game and then fire it in a direction
 * each attack keeps track of the bullets that it fires, and each projectile should have a reference
 * back to the attack that launched it
 */

 import 'phaser';
 import Vec2d from '../math/vec2d';
 import ActionSprite from '../sprites/actionsprite';
 import Attack from './attack';
 import NMath from '../math/math';
 import { GameScene } from '../scenes/gamescene';

 class RangedAttack extends Attack
 {
   constructor(name: string, anim: string, hitBox: Vec2d, offset: Vec2d, zTol: number, lockTime: number, damage: number, owner: ActionSprite)
   {
     super(name, anim, hitBox, offset, zTol, lockTime, damage, owner);
   }

   public attack()
   {
     this.owner.lockDown(this.getLockTime());
     this.owner.setAttacking();
   }
 }

 export default RangedAttack;
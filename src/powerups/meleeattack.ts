import 'phaser';
import Vec2d from '../math/vec2d';
import ActionSprite from '../sprites/actionsprite';
import Attack from './attack';
import NMath from '../math/math';
import { GameScene } from '../scenes/gamescene';

class MeleeAttack extends Attack
{
  constructor(name: string, anim: string, hitBox: Vec2d, offset: Vec2d, zTol: number, lockTime: number, damage: number, owner: ActionSprite)
  {
    super(name, anim, hitBox, offset, zTol, lockTime, damage, owner);
  }

  public attack()
  {
    //just need to fire this out there and let it go as it goes
    const position = this.owner.getPosition();
    //lock the owner down
    this.owner.lockDown(this.getLockTime());
    this.owner.setAttacking();
    
    //loop through evnerything at once
    const sprites = GameScene.sceneSprites;
    for( var key in sprites)
    {
      const sprite = sprites[key];

      if(sprite instanceof ActionSprite) {
        if(NMath.zCheck(this.owner.getZ(), sprite.getZ(), this.zTol)) {
          //they are close enough on the z axis ( same as before )

          //do the little box because im like that
          /*
          if(this.owner.isLeft()) {
            GameScene.HitTest.setPosition(position.x + this.offset.x - 50, position.y + this.offset.y);
          } else {
            GameScene.HitTest.setPosition(position.x + this.offset.x, position.y + this.offset.y);
          }
          */          

          //make sure its not us 
          if(this.notMe(sprite)) {
            //we punched someone or kicked or something we're good to apply damage or something
            if(NMath.BoxCollide(position, this.hitBox, sprite.getPosition(), sprite.hitCollider(), this.owner.isLeft())) {
              console.log(`${this.owner.getName()} just punched ${sprite.getName()}`);
            }
          }
        }
      }
    }
  }
}

export default MeleeAttack;
import 'phaser';
import Vec2d from '../math/vec2d';
import PhysicSprite from '../sprites/physicsprite';
import ActionSprite from '../sprites/actionsprite';
import Attack from './attack';
import NMath from '../math/math';
import { GameScene } from '../scenes/gamescene';

class GrappleAttack extends Attack
{
  protected target: PhysicSprite;
  protected holdTime: number; // how long can you hold on ?


  constructor(name: string, anim: string, hitBox: Vec2d, offset:Vec2d, zTol: number, lockTime: number, damage: number, owner: ActionSprite, holdTime: number)
  {
    super(name, anim, hitBox, offset, zTol, lockTime, damage, owner);
    this.target = null;
  }

  public attack()
  {
    //check if there's anything in the hitbox if there is pick the closest one and thats 
    //the target let the lower classes handle the rest
    //lock down the owner
    this.owner.lockDown(this.getLockTime());
    if(this.target) {
      //we have a target we should honestly just throw them or something
    }
    else {
      const position = this.owner.getPosition();
      //check in the box
      
      //loop through everything ONCE
      const sprites = GameScene.sceneSprites;
      for( var key in sprites) 
      {
        //the working sprite
        const sprite = sprites[key];
        //run a z check
        if(sprite instanceof PhysicSprite) {
          if(NMath.zCheck(this.owner.getZ(), sprite.getZ(), this.zTol)) {
            //they are close enough on the z axis ( this is very important )
            //draw a box the size of the collision
  
            //now we can safely check for a sprite hit box collision
            if(NMath.BoxCollide(position, this.hitBox, sprite.getPosition(), sprite.hitCollider(), this.owner.isLeft())) {
              //we got EM check if we have a target if we don't we're done
              //make sure its not us
              if(this.notMe(sprite)) {
                if(!this.target) {
                  //check the weight we can't pick up big boys
                  if(this.owner.getStrength() >= sprite.getWeight()) {
                    this.target = sprite;
                  }                  
                }
                else {
                  //welp lets see how close we are to the owner
                  //draw a rectangle as big as our collision box
                  if( Vec2d.DISTANCE(this.owner.getPosition(), 
                      sprite.getPosition()) > Vec2d.DISTANCE(this.owner.getPosition(),
                      this.target.getPosition()) ) {
                    this.target = sprite;
                  }
                }
              }
            }
          }
        }
      }
      if(this.target !== null) {
        this.target.setHeld(true);
        this.owner.pickup(this.target);
      }
    }
    //we should have a target now ( unless we didn't hit anything )
  }
}

export default GrappleAttack;
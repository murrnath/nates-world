import 'phaser';
import Nate from '../sprites/nate';
import PhysicsObject from '../sprites/physicsobjects/physicsobject';
import Vec2d from '../math/vec2d';
import Background from '../sprites/backgrounds';
import ActionSprite from '../sprites/actionsprite';
import PhysicSprite from '../sprites/physicsprite';
import Granny from '../sprites/enemies/granny';
import Projectile from '../sprites/projectiles/projectile';

export class GameScene extends Phaser.Scene {
  public static readonly GAMETOP = 340;
  public static readonly FLYTOP = -3000;
  public static readonly GRAVITY = 1;
  public static SCREENRATIO: number;
  public static sceneSprites: Array<ActionSprite>;
  public static sceneProjectiles: Array<Projectile>;
  public static player: ActionSprite;
  public static HitTest: Phaser.GameObjects.Sprite;
  private lastTime: number;

  //clas variables
  delta:number;
  info: Phaser.GameObjects.Text;
  nate: Nate;
  granny: Granny;
  car: PhysicsObject;
  box: PhysicsObject;
  backgrounds: Array<Background>;
  cursors;
  camera: Phaser.Cameras.Scene2D.Camera;

  pointer;

  constructor() {
    super({
      key: 'GameScene'
    });
  }

  init(params): void {
    this.delta = 1000;
    this.backgrounds = new Array<Background>();
    GameScene.sceneSprites = new Array<ActionSprite>();
    GameScene.sceneProjectiles = new Array<Projectile>();
  }

  public getPlayer() : ActionSprite
  {
    return this.nate;
  }

  preload(): void {
    //load up images here if we had any

    //check the screen size and then load different sized assets
    
    GameScene.SCREENRATIO = Math.floor(window.devicePixelRatio);

    console.log(`loading ${GameScene.SCREENRATIO}x assets`);

    //load nate in all of his greatness
    this.load.multiatlas('nate', 'assets/nate.json', 'assets');

    //load all of the eneimes I want to beatup
    this.load.multiatlas('granny', 'assets/granny.json', 'assets');

    this.load.image("dropshadow", `assets/dropshadow@${GameScene.SCREENRATIO}x.png`);
    this.load.image("sky", `assets/sky@${GameScene.SCREENRATIO}x.jpg`);
    this.load.image('road', `assets/road@${GameScene.SCREENRATIO}x.jpg`);
    this.load.image('flyeffect', `assets/fly-texture@${GameScene.SCREENRATIO}x.png`);
    this.load.image('flyeffectl', `assets/fly-texture-l@${GameScene.SCREENRATIO}x.png`);
    this.load.image('hittest', `assets/hit@${GameScene.SCREENRATIO}x.jpg`);
    this.load.image('bluecar', `assets/car@${GameScene.SCREENRATIO}x.png`);
    this.load.image('box', `assets/box@${GameScene.SCREENRATIO}x.png`);
  }

  create(): void 
  {
    //create all our objects here
  
    this.setupScene();
    this.pointer = this.input.activePointer;
  }

  update(time: number): void {
    //calculate the delta time its in milliseconds

    const delta = time - this.lastTime;
    this.lastTime = time;

    //game update this is going to be a very basic game
    
    for( var key in this.backgrounds) {
      this.backgrounds[key].tick();
    }

    for( var key in GameScene.sceneSprites) {
      const sprite = GameScene.sceneSprites[key];

      sprite.tick(this.cursors, delta);
    }

    for( var key in GameScene.sceneProjectiles) {
      const proj = GameScene.sceneProjectiles[key];

      proj.tick(null, delta);
    }
  }

  private setupInput() : void
  {
    this.cursors = this.input.keyboard.addKeys({
      up: Phaser.Input.Keyboard.KeyCodes.UP,
      down: Phaser.Input.Keyboard.KeyCodes.DOWN,
      left: Phaser.Input.Keyboard.KeyCodes.LEFT,
      right: Phaser.Input.Keyboard.KeyCodes.RIGHT,
      jump: Phaser.Input.Keyboard.KeyCodes.S,
      fly: Phaser.Input.Keyboard.KeyCodes.W,
      super: Phaser.Input.Keyboard.KeyCodes.E,
      attack: Phaser.Input.Keyboard.KeyCodes.D
    });
  }

  private setupBackground() : void
  {
    this.backgrounds.push(new Background(this, 0, 0, 'sky', new Vec2d(0.2, 0.2)));
    this.backgrounds.push(new Background(this, 0, 222, 'road', new Vec2d(1, 0), new Vec2d(0, 1)));
  }

  private setupPlayer() : void
  {
    this.nate = new Nate(this, 300, 300);
    GameScene.player = this.nate;
  }

  private setupEnemies() : void 
  {
    this.granny = new Granny(this, 700, 400);
  }

  private setupScene() : void 
  {
    //do stuff a bit later
    this.setupBackground();
    this.setupPlayer();
    this.setupEnemies();
    this.setupInput();

    this.camera = this.cameras.main;
    this.camera.startFollow(this.nate.getSprite());

    for(var key in this.backgrounds) 
    {
      this.backgrounds[key].setFollow(this.nate);
    }

    GameScene.HitTest = this.add.sprite(0, 0, 'hittest');
    GameScene.HitTest.setOrigin(0, 0.5);
    this.children.bringToTop(GameScene.HitTest);

    this.car = new PhysicsObject(this, 2300, 500, 'bluecar', PhysicSprite.MEDIUM, new Vec2d(280, 145));

    this.box = new PhysicsObject(this, 2000, 500, 'box', PhysicsObject.MEDIUM, new Vec2d(256, 256));

    this.lastTime = 0;
  }
};
import Vec2d from '../math/vec2d';
import {GameScene} from '../scenes/gamescene';
import ActionSprite from '../sprites/actionsprite';
import Background from '../sprites/background';

class BackgroundManager
{
  protected backgrounds:Array<Background>;

  private player: ActionSprite;

  public static readonly CENTER = 'center';
  public static readonly CENTERLEFT = 'center-left';
  public static readonly CENTERRIGHT = 'center-right';

  public static readonly BGWIDTH = 1000;

  private mapPosition: Vec2d;
  private scrollSpeed: Vec2d;


  constructor(scene: GameScene, ss:Vec2d)
  {
    this.backgrounds = new Array<Background>();
    this.scrollSpeed = ss;
  }

  public tick()
  {
    //based on the players speed we need to shuffle the images around
    //position based on the nate map position im going to offset everything by it
    
    if(this.player) {     
      const center = this.getBackground(BackgroundManager.CENTER);
      const width = center.getSprite().width / 2;
      const height = center.getSprite().height / 2;

      this.mapPosition = this.mapPosition.plus(this.player.getSpeed());
      const relativeX = ((0 - this.mapPosition.x) * this.scrollSpeed.x) % width;
      const relativeY = ((0 - this.mapPosition.y) * this.scrollSpeed.y) % height;
      
      this.setBackgroundImageXPosition(BackgroundManager.CENTER, this.mapPosition.x + relativeX);
      this.setBackgroundImageXPosition(BackgroundManager.CENTERLEFT, this.mapPosition.x + relativeX - width);
      this.setBackgroundImageXPosition(BackgroundManager.CENTERRIGHT, this.mapPosition.x + relativeX + width);

      const log1 = this.getBackground(BackgroundManager.CENTER).getPosition().x;
      const log2 = this.getBackground(BackgroundManager.CENTERLEFT).getPosition().x;
      const log3 = this.getBackground(BackgroundManager.CENTERRIGHT).getPosition().x;

      this.setBackgroundImageYPosition(BackgroundManager.CENTER, this.mapPosition.y + relativeY);
      this.setBackgroundImageYPosition(BackgroundManager.CENTERLEFT, this.mapPosition.y + relativeY - height);
      this.setBackgroundImageYPosition(BackgroundManager.CENTERRIGHT, this.mapPosition.y + relativeY + height);

      for(var key in this.backgrounds) {
        if (this.backgrounds.hasOwnProperty(key)) {
          this.backgrounds[key].setOffset(this.player.getDisplayPosition());
          this.backgrounds[key].tick(null);
        }
      }
    }
  }

  public setFollow(player: ActionSprite)
  {
    this.player = player;
    const center = this.getBackground(BackgroundManager.CENTER);
    this.mapPosition = new Vec2d(center.getPosition().x - this.player.getSprite().x, 0);
  }

  private setBackgroundImageXPosition(key: string, num: number) {
    const bg = this.getBackground(key);
    bg.setPosition(new Vec2d(num - this.player.getPosition().x, bg.getPosition().y));
  }

  private setBackgroundImageYPosition(key: string, num: number) {
    const bg = this.getBackground(key);
    bg.setPosition(new Vec2d(bg.getPosition().x, num));
  }

  public getBackground(key: string): Background {
    return this.backgrounds[key];
  }

  public addBackground(scene: GameScene, key: string, texture: string, position: Vec2d) : void
  {
    //create a background sprite and add it to the array
    const bg = new Background(scene, position.x, position.y, texture);

    this.backgrounds[key] = bg;
  }

  public removeBackground(key: string) : void
  {
    if(key in this.backgrounds)
    {
      delete this.backgrounds[key];
    }
  }
}

export default BackgroundManager;
import Ai from './artificalintelligence';
import { GameScene } from '../scenes/gamescene';
import Enemy from '../sprites/enemies/enemy';

class RangedEnemy extends Ai
{

  constructor(scene: GameScene, owner: Enemy)
  {
    super(scene, owner);
    this.attackRange = Ai.LONGRANGE;
  }

  public tick(delta: number)
  {
    super.tick(delta);
  }
}

export default RangedEnemy;
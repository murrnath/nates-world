import { GameScene } from '../scenes/gamescene';
import ActionSprite from '../sprites/actionsprite';
import Enemy from '../sprites/enemies/enemy';
import Vec2d from '../math/vec2d';

class Ai 
{
  protected static readonly GRABRANGE = 20;
  protected static readonly MELEERANGE = 50;
  protected static readonly LONGRANGE = 200;

  protected scene: GameScene;
  protected owner: Enemy;
  protected target: ActionSprite;

  protected moveSpeed: number;
  protected targetLocation: Vec2d;
  protected targetZ: number;
  protected attackRange: number;

  protected aiTimer: number;
  protected canAttack: boolean;

  constructor(scene: GameScene, owner: Enemy)
  {
    this.scene = scene;
    this.owner = owner;
    this.target = scene.getPlayer();
    this.moveSpeed = 5;
    this.aiTimer = 2000;
    this.targetLocation = this.owner.getPosition();
    this.canAttack = false;
  }

  public tick(delta: number)
  {
    //aii logic goes here
    this.aiTimer -= delta;
    // the VERY first thing i want to do is make sure that we are facing the right way
    // figure out if target is on the left or the right of us
    if(this.aiTimer <= 0) {
      this.aiUpdate();
      this.aiTimer = 2000;
    }

    this.everyUpdate();
  }

  protected aiUpdate()
  {
    this.tryToMatchZ();
  }

  protected everyUpdate()
  {
    //we will have target positions to get to and then we can attack
    let addspeed = new Vec2d(0, 0);

    if(this.owner.getPosition().y > this.targetLocation.y) {
      if(this.owner.getPosition().y - this.targetLocation.y <= this.moveSpeed && this.owner.getPosition().y - this.targetLocation.y >= -this.moveSpeed) {
        this.owner.setPosition(new Vec2d(this.owner.getPosition().x, this.targetLocation.y));
      } else {
        addspeed.y = -this.moveSpeed;
      }
    } else if (this.owner.getPosition().y < this.targetLocation.y) {
      if(this.owner.getPosition().y - this.targetLocation.y <= this.moveSpeed && this.owner.getPosition().y - this.targetLocation.y >= -this.moveSpeed) {
        this.owner.setPosition(new Vec2d(this.owner.getPosition().x, this.targetLocation.y));
      } else {
        addspeed.y = this.moveSpeed;
      }
    }

    if(this.owner.getPosition().x > this.targetLocation.x) {
      if(this.owner.getPosition().x - this.targetLocation.x <= this.moveSpeed && this.owner.getPosition().x - this.targetLocation.x >= -this.moveSpeed) {
        this.owner.setPosition(new Vec2d(this.targetLocation.x, this.owner.getPosition().y));
      } else {
        addspeed.x = -this.moveSpeed;
      }
    } else if (this.owner.getPosition().x < this.targetLocation.x) {
      if(this.owner.getPosition().x - this.targetLocation.x <= this.moveSpeed && this.owner.getPosition().x - this.targetLocation.x >= -this.moveSpeed) {
        this.owner.setPosition(new Vec2d(this.targetLocation.x, this.owner.getPosition().y));
      } else {
        addspeed.x = this.moveSpeed;
      }
    }

    this.updateFacingDirection();
    this.owner.setSpeed(addspeed); 

    this.canAttack = this.owner.getPosition().equals(this.targetLocation);
  }

  protected updateFacingDirection()
  {
    if(this.target.getPosition().x > this.owner.getPosition().x) {
      //the player is to the right of us
      this.owner.faceLeft(true);
    } else {
      this.owner.faceLeft(false);
    }
  }

  public isCanAttack()
  {
    return this.canAttack;
  }

  protected tryToMatchZ()
  {
    //lets check our z if its lower we should move down if its higher we should move up

    const offset = (this.owner.isLeft() ? -this.attackRange : this.attackRange);
    
    this.targetLocation = this.target.getPosition().plus(new Vec2d(offset, 0));
  }
}

export default Ai;
import PhysicsSprite from '../physicsprite';
import { GameScene } from '../../scenes/gamescene';
import Vec2d from '../../math/vec2d';

class PhysicsObject extends PhysicsSprite
{
  private static index = 1;
  private hitSize: Vec2d;

  constructor(scene: GameScene, x: number, y: number, texture: string, weight: number, hitSize: Vec2d)
  {
    super(scene, x, y, texture, (texture + PhysicsObject.index++), null, true);
    this.weight = weight;
    this.hitSize = hitSize;
  }

  public hitCollider() : Vec2d
  {
    return this.hitSize;
  }

  public tick(cursor = null, delta = null)
  {
    super.tick(cursor, delta);
  }
}

export default PhysicsObject;
import 'phaser';
import Sprite from './sprite';
import { GameScene } from '../scenes/gamescene';
import Vec2d from '../math/vec2d';

class Background extends Sprite
{
  constructor(scene: GameScene, x: number, y: number, texture: string)
  {
    super(scene, x, y, texture);
  }

  public tick(cursors = null) {
    this.sprite.setPosition(this.position.x + this.displayOffset.x, this.position.y + this.displayOffset.y);
  }
}

export default Background;
import Sprite from "./sprite";
import Vec2d from "../math/vec2d";
import { GameScene } from "../scenes/gamescene";

/**
 * in between action sprite and normal sprite it can move but thats about it
 * maybe ill do physics sprite based off of this as well
 */

class MoveableSprite extends Sprite
{
  protected y_pos: number;
  protected y_speed: number;
  protected speed: Vec2d;
  protected facingLeft: boolean;

  constructor(scene: GameScene, x: number, y: number, texture: string, name: string, anim?: string, dropshadow: boolean = true)
  {
    super(scene, x, y, texture, name, anim, dropshadow);
    this.speed = Vec2d.ZERO();
    this.y_speed = 0;
    this.y_pos = y;
    this.sprite.setPosition(this.position.x, this.y_pos);
    this.dropshadow.setPosition(this.position.x, this.position.y);
  }

  public tick(cursors = null, delta = null)
  {
    this.y_pos += this.y_speed;
    this.position = this.position.plus(this.speed);

    this.sprite.setPosition(this.position.x, this.y_pos);
    this.updateLeft();

    this.dropshadow.setPosition(this.position.x, this.position.y);
    this.calculateAnimationState();
  }

  protected updateLeft() 
  {
    if(this.speed.x != 0) {
      this.facingLeft = this.speed.x < 0; 
    }
  }

  public faceLeft(left: boolean)
  {
    this.facingLeft = left;
  }
}

export default MoveableSprite;
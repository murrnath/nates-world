import 'phaser';
import Vec2d from '../math/vec2d';
import { GameScene } from '../scenes/gamescene';
import ActionSprite from '../sprites/actionsprite';

class Background 
{
  protected readonly width = 1000;
  protected readonly height = 562;
  protected scrollSpeed: Vec2d;
  protected position: Vec2d;
  protected sprite: Phaser.GameObjects.TileSprite;
  protected follow: ActionSprite;
  
  constructor(scene: Phaser.Scene, x: number, y: number, texture: string, scrollSpeed: Vec2d, scrollFactor: Vec2d = Vec2d.ZERO())
  {
    this.sprite = scene.add.tileSprite(x, y, this.width, this.height, texture);
    this.scrollSpeed = scrollSpeed;
    this.position = new Vec2d(x, y);
    this.sprite.setOrigin(0, 0);

    this.sprite.scrollFactorX = scrollFactor.x;
    this.sprite.scrollFactorY = scrollFactor.y;
  }

  public setFollow(newPl: ActionSprite)
  {
    this.follow = newPl;
  }

  public tick()
  {
    if(this.follow)
    {
      const moveSpeed = this.follow.getSpeed();
      this.sprite.tilePositionX += moveSpeed.x * this.scrollSpeed.x;
      this.sprite.tilePositionY += moveSpeed.y * this.scrollSpeed.y;
    }
  }
}

export default Background;
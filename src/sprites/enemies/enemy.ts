import 'phaser';
import Vec2d from '../../math/vec2d';
import ActionSprite from '../actionsprite';
import ArtificalIntelligence from '../../services/artificalintelligence';
import { GameScene } from '../../scenes/gamescene';

class Enemy extends ActionSprite
{
  protected aiModule: ArtificalIntelligence;

  constructor(scene: GameScene, x: number, y: number, texture: string, name: string, anim?: string, dropshadow: boolean = true)
  {
    super(scene, x, y, texture, name, anim, dropshadow);
  }

  public tick(cursors = null, delta: number)
  {
    this.aiModule.tick(delta);
    
    super.tick(null, delta);
  }

  protected updateLeft()
  {
    
  }
}

export default Enemy;
import 'phaser';
import Vec2d from '../../math/vec2d';
import Enemy from './enemy';
import { GameScene } from '../../scenes/gamescene';
import Attack from '../../powerups/attack';
import ThrowPurse from '../../attacks/throwpurse';
import RangedEnemy from '../../services/rangedenemy';

class Granny extends Enemy
{
  private static readonly IDLE = 'granny_idle';
  private static readonly THROWPURSE = 'granny_purse';

  private static grannydex = 0;

  private purseTimer: number;

  constructor(scene: GameScene, x: number, y: number)
  {
    //probably going to change some of that
    super(scene, x, y, 'granny', `granny${Granny.grannydex++}`, 'granny_idle/001.png', true);
    this.aiModule = new RangedEnemy(scene, this);
    this.createAnimations(scene);
    this.setupAttacks();
    this.resetPurse();
  }

  public tick(cursors = null, delta: number)
  {
    super.tick(cursors = null, delta);

    //just make her throw the purse every 2 seconds
    if(!this.attacking && this.purseTimer <= 0 && this.aiModule.isCanAttack()) {
      //throw the purse
      this.attacks['throw_purse'].attack();
      this.resetPurse();
    } else {
      this.purseTimer -= delta;
    }

    if(this.attackTimer <= 0) {
      this.attacking = false;
    }
  }

  private resetPurse()
  {
    this.purseTimer = 2000;
  }

  protected createAnimations(scene: GameScene)
  {
    this.createAnim(scene, 'granny', 1, 1, 'granny_idle/', 10, -1, Granny.IDLE);
    this.createAnim(scene, 'granny', 1, 2, 'granny_throw/', 5, 0, Granny.THROWPURSE);
  }

  protected calculateAnimationState() 
  {
    
    if(this.attacking) {
      this.sprite.play(Granny.THROWPURSE, true);
    } else {
      this.sprite.play(Granny.IDLE, true);
    }
  }

  private setupAttacks()
  {
    this.attacks = new Array<Attack>();
    this.attacks['throw_purse'] = new ThrowPurse('throw_purse', Granny.THROWPURSE, this, new Vec2d(0, -70));
  }
}

export default Granny;
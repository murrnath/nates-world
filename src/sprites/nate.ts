import ActionSprite from './actionsprite';
import Vec2d from '../math/vec2d';
import { GameScene } from '../scenes/gamescene';
import Attack from '../powerups/attack';
import PickUpObject from '../attacks/pickupobject';
import Punch from '../attacks/punch';
import MegaPunch from '../attacks/megapunch';
import ComboAttack from '../powerups/comboattack';

class Nate extends ActionSprite
{
  private static readonly IDLE = 'nate_idle';
  private static readonly BATTLEIDLE = 'nate_battle_idle';
  private static readonly WALK = 'nate_walk';
  private static readonly RUN = 'nate_run';
  private static readonly JUMPUP = 'nate_jump_up';
  private static readonly JUMPDOWN = 'nate_jump_down';
  private static readonly LEFTPUNCH = 'nate_l_punch';
  private static readonly RIGHTPUNCH = 'nate_r_punch';
  private static readonly MEGAPUNCH = 'nate_mega_punch';
  private static readonly SUPERIDLE = 'super_nate_idle';
  private static readonly SUPERRUN = 'super_nate_run';
  private static readonly SUPERFLY = 'super_nate_fly';
  private static readonly SUPERLAND = 'super_nate_land';

  private superNate: boolean;
  private flyctrl: boolean;
  private flying: boolean;
  private flyStreakRight: Phaser.GameObjects.Particles.ParticleEmitter;
  private flyStreakLeft: Phaser.GameObjects.Particles.ParticleEmitter;
  private streakOffset: Vec2d;

  private canSuper: boolean;
  
  constructor(scene: GameScene, x: number, y: number) 
  {
    super(scene, x, y, 'nate', 'nate', 'nate_idle/001.png', true);

    this.superNate = false;
    this.flying = false;
    this.canSuper = false;

    this.name = "nate";

    this.setupAttacks();

    this.strength = ActionSprite.MEDIUM;

    const particle = scene.add.particles('flyeffect');
    const lParticle = scene.add.particles('flyeffectl');

    this.flyStreakRight = particle.createEmitter({
      speed: 10,
      scale: { start: 0.4, end: 0 },
      alpha: 0.1,
      blendMode: 'ADD'
    });

    this.flyStreakLeft = lParticle.createEmitter({
      speed: 10,
      scale: { start: 0.4, end: 0 },
      alpha: 0.1,
      blendMode: 'ADD'
    });
    

    this.flyStreakRight.stop();

    this.createAnimations(scene);

    this.holdOffset = new Vec2d(20, -140);

    scene.children.bringToTop(this.sprite);
  }

  public tick(cursors, delta: number) 
  {
    //check for left right up and down only going to do left and right
    this.checkKeyInput(cursors);

    if(this.flying)
    {
      if(!this.held === true) {
        this.flyupdate(delta);
      }
    }
    else
    {
      super.tick(cursors);
    }
    this.calculateAnimationState();
    this.updateEmitterState();

    if(this.attackTimer > 0) {
      this.attackTimer -= delta;
    } else {
      this.attacking = false;
    }

    this.attacks['punch'].tick(delta);
  }

  private checkKeyInput(cursors)
  {
    this.speed = Vec2d.ZERO();

    if(this.attackTimer <= 0) 
    {
      if(this.canSuper)
      {
        if(cursors.super.isDown)
        {
          this.superNate = true;
        }
      }
      if(cursors.left.isDown)
      {
        this.speed.x -= 10;
      }
      if(cursors.right.isDown)
      {
        this.speed.x += 10;
      }
      if(cursors.up.isDown)
      {
        this.speed.y -= 10;
        if(!this.grounded && this.position.y > 360) 
        {
          this.y_pos -= 10;
        }
      }
      if(cursors.down.isDown)
      {
        this.speed.y += 10;

        if (this.flying === true) {
          if (cursors.jump.isDown) {
            this.flying = false;
            this.y_speed += 7;
          }
        }
        else if (!this.grounded && this.position.y < 562) {
          this.y_pos += 10;
        }
      }
      if (!this.flying) {
        if (cursors.jump.isDown) {
          if (this.grounded === true) {
            this.grounded = false;
            this.y_speed -= 20;
          }
        }
      }
      if(this.superNate)
      {
        if (cursors.fly.isDown) {
          if (this.flying) {
            this.flyctrl = true;
          }
          else {
            this.flying = true;
          }
        }
        if (cursors.fly.isUp) {
          this.flyctrl = false;
        }
      }

      if(cursors.attack.isDown)
      {
        if(this.attackTimer <= 0) {
          this.handleAttacks();
        }
      }
    }
    
  }

  private handleAttacks() : void
  {
    if(this.superNate)
    {
      //handle all of the super attacks
      //do the super attacks
      /*
      const grabbed = this.superAttacks['grab'].attack();
      if(!grabbed) {
        //we can do our other attacks here

      }
      */
     this.battleStance = true;
    }
    else 
    {
      //handle all of the poopy attacks
      //const grabbed = this.attacks['grab'].attack();
      //this.latestAttack = this.attacks['grab'];
      if(true) {
        //we  can do our other attacks
        this.attacks['punch'].attack();
        this.latestAttack = this.attacks['punch'];
        this.setStance();
      }
    }
  }

  public getSpeed() : Vec2d
  {
    if(this.flying) {
      //return new Vec2d(this.speed.x, this.y_speed);
      return new Vec2d(this.speed.x, this.speed.y);
    }
    return new Vec2d(this.speed.x, this.y_speed);
  }

  private flyupdate(delta: number) {

    if(this.flyctrl === false)
      this.position = this.position.plus(this.speed);
    else
      this.position.x += this.speed.x;
    
    this.y_pos += this.speed.y;

    if (this.position.y > 562) {
      this.position.y = 562;
    }

    if (this.position.y - this.walkCollider.y < GameScene.GAMETOP) {
      this.position.y = GameScene.GAMETOP + this.walkCollider.y;
    }

    if(this.y_pos - this.walkCollider.y < GameScene.FLYTOP) {
      this.y_pos = GameScene.FLYTOP + this.walkCollider.y;
      this.speed.y = 0;
    }

    if (this.y_pos > this.position.y && this.speed.y > 1 ) {
      this.grounded = true;
      this.flying = false;
      this.y_speed = 0;
      this.speed.y = 0;
      this.y_pos = this.position.y;
    }

    if (this.attackTimer > 0) {
      this.attackTimer -= delta;
    }

    this.sprite.setPosition(this.position.x + this.displayOffset.x, this.y_pos + this.displayOffset.y);
    this.dropshadow.setPosition(this.position.x, this.position.y);
    
    this.flyStreakRight.setPosition(this.position.x + this.displayOffset.x + 
      this.streakOffset.x, this.y_pos + this.displayOffset.y + this.streakOffset.y);

    this.flyStreakLeft.setPosition(this.position.x + this.displayOffset.x + 
      this.streakOffset.x, this.y_pos + this.displayOffset.y + this.streakOffset.y);

      this.updateHolding();
  }

  protected calculateAnimationState()
  {
    if(this.superNate)
    {
      if(this.flying)
      {
        if(this.flyctrl && this.speed.y > 0)
        {
          this.sprite.play(Nate.SUPERLAND);
        }
        else 
        {
          this.sprite.play(Nate.SUPERFLY);
        }
      }
      else 
      {
        if(!this.grounded && this.y_speed > 0)
        {
          this.sprite.play(Nate.SUPERLAND, true);
        }
        else
        {
          this.sprite.play(Nate.SUPERIDLE, true);
        }
      }
    }
    else {
      if(this.grounded) {
        if(this.attacking) {
          this.sprite.play(this.latestAttack.getAnim(), true);
        } else if(this.speed.x != 0) {
          this.sprite.play(Nate.RUN, true);
        } else {
          if(this.battleStance) {
            this.sprite.play(Nate.BATTLEIDLE, true);
          } else {
            this.sprite.play(Nate.IDLE, true);
          }
        }
      } else {
        //we are in the air lets see if we are jumping up or we are jumping down
        if(this.y_speed <= 0) {
          //we're going up
          this.sprite.play(Nate.JUMPUP, true);
        } else {
          //we're coming back down
          this.sprite.play(Nate.JUMPDOWN, true);
        }
      }
    }
  }

  private updateEmitterState()
  {
    if(this.superNate && this.flying && !this.speed.equals(Vec2d.ZERO())) {
      if(this.facingLeft && this.speed.x < 0) {
        this.flyStreakLeft.start();
        this.flyStreakRight.stop();
      } else {
        this.flyStreakRight.start();
        this.flyStreakLeft.stop();
      }
    }
    else {
      this.flyStreakRight.stop();
      this.flyStreakLeft.stop();
    }
  }

  protected createAnimations(scene: GameScene)
  {
    this.createAnim(scene, 'nate', 1, 1, 'nate_idle/', 10, -1, Nate.IDLE);
    this.createAnim(scene, 'nate', 1, 1, 'nate_l_punch/', 10, -1, Nate.BATTLEIDLE);
    this.createAnim(scene, 'nate', 1, 2, 'nate_walk/', 10, -1, Nate.WALK);
    this.createAnim(scene, 'nate', 1, 3, 'nate_run/', 10, -1, Nate.RUN);
    this.createAnim(scene, 'nate', 1, 2, 'nate_jump_up/', 5, 0, Nate.JUMPUP);
    this.createAnim(scene, 'nate', 1, 1, 'nate_jump_down/', 5, 0, Nate.JUMPDOWN);
    this.createAnim(scene, 'nate', 1, 2, 'nate_l_punch/', 5, 0, Nate.LEFTPUNCH);
    this.createAnim(scene, 'nate', 1, 2, 'nate_r_punch/', 5, 0, Nate.RIGHTPUNCH);
    this.createAnim(scene, 'nate', 1, 2, 'nate_mega_punch/', 10, 0, Nate.MEGAPUNCH);
    this.createAnim(scene, 'nate', 1, 1, 'super_nate_idle/', 10, -1, Nate.SUPERIDLE);
    this.createAnim(scene, 'nate', 1, 3, 'super_nate_run/', 10, -1, Nate.SUPERRUN);
    this.createAnim(scene, 'nate', 1, 1, 'super_nate_land/', 10, -1, Nate.SUPERLAND);
    this.createAnim(scene, 'nate', 1, 1, 'super_nate_fly/', 10, -1, Nate.SUPERFLY);
  }

  private setupAttacks()
  {
    const atks = new Array<Attack>();
    atks['lpunch1'] = new Punch('lpunch1', Nate.LEFTPUNCH , this, new Vec2d(0, -75));
    atks['rpunch1'] = new Punch('rpunch1', Nate.RIGHTPUNCH, this, new Vec2d(0, -75));
    atks['lpunch2'] = new Punch('lpunch2', Nate.LEFTPUNCH, this, new Vec2d(0, -75));
    atks['rpunch2'] = new Punch('rpunch2', Nate.RIGHTPUNCH, this, new Vec2d(0, -75));
    atks['megapunch'] = new MegaPunch('megapunch', Nate.MEGAPUNCH, this, new Vec2d(0, -75));

    this.attacks = new Array<Attack>();
    this.attacks['grab'] = new PickUpObject('pickupcar', 'nate', this, new Vec2d(0, -75));
    this.attacks['punch'] = new ComboAttack(atks, 150);


    this.streakOffset = new Vec2d(12, -89);
    this.latestAttack = null;
  }
}

export default Nate;
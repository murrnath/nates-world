import 'phaser';
import ActionSprite from '../sprites/actionsprite';
import Vec2d from '../math/vec2d';
import { GameScene } from '../scenes/gamescene';

class PhysicSprite extends ActionSprite
{
  public static readonly LIGHT = 1;
  public static readonly MEDIUM = 2;
  public static readonly HEAVY = 3;
  public static readonly RIDICULOUS = 4;

  protected held: boolean;
  protected weight: number;
  
  constructor(scene: GameScene, x: number, y: number, texture: string, name: string, anim?: string, dropwshadow: boolean = true)
  {
    super(scene, x, y, texture, name, anim, dropwshadow);

    this.held = false;
  }

  public tick(cursors = null, delta: number = null)
  {
    //these objects don't walk themselves and thats because you can throw them
    //we need to apply gravity to these objects
    if(this.held != true) {
      this.y_speed += GameScene.GRAVITY;
      this.position = this.position.plus(this.speed);
      this.y_pos += this.y_speed;

      this.physics_step();
      this.forceInScreen();
      this.updateSpriteLocation();
    } else {
      this.updateSpriteLocation();
    }
  }

  

  public physics_step() : void
  {
    //we need to slow down by a tiny amount every frame and make sliding things slide
    if(this.grounded)
    {
      //we're on the ground slow to a stop
      //are we going left or right
      if(this.speed.x !== 0)
      {
        if((this.speed.x < 0.5 && this.speed.x > -1) || (this.speed.x > 0.5 && this.speed.x < 1)) {
          this.speed.x = 0;
          //close enough just stop it now
        }
        if(this.speed.x < 0) 
        {
          //we're going left
          this.speed.x += 0.5;
        }
        else if(this.speed.x > 0)
        {
          this.speed.x -= 0.5;
        }
      }
    }
    else
    {
      //we're in the air do some air stuff or something
    }
  }

  public setSpeed(val: Vec2d) : void
  {
    this.speed = val;
  }

  public getSpeed() : Vec2d
  {
    return this.speed;
  }

  public setHeld(val: boolean) : void 
  {
    this.held = val;
  }

  public getHeld() : boolean
  {
    return this.held;
  }

  public thrown(tSpeed: Vec2d, offset: number)
  {
    //set the y_speed and x speed
    this.speed.x = tSpeed.x;
    this.y_speed = tSpeed.y;
    this.y_pos += offset;
    this.grounded = false;
  }

  public getWeight() : number
  {
    return this.weight;
  }
}

export default PhysicSprite;
import Projectile from "./projectile";
import { GameScene } from "../../scenes/gamescene";
import Vec2d from "../../math/vec2d";
import ActionSprite from "../actionsprite";

class Purse extends Projectile
{
  private static pursdex: number;
  protected owner: ActionSprite;

  constructor(scene: GameScene, x: number, y: number, z:number, damage: number, speed: Vec2d, owner: ActionSprite)
  {
    super(scene, x, y, z, 'granny', `granny${Purse.pursdex++}`, speed, damage, 10, Projectile.LIFE_MEDIUM, 'granny_purse/001.png');
    this.owner = owner;
  }

  public tick(cursors = null, delta = null)
  {
    super.tick(cursors, delta);
  }
}

export default Purse;
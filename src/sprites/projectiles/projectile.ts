import Sprite from '../moveablesprite';
import { GameScene } from '../../scenes/gamescene';
import Vec2d from '../../math/vec2d';

class Projectile extends Sprite
{
  public static readonly LIFE_SHORT = 3000;
  public static readonly LIFE_MEDIUM = 5000;
  public static readonly LIFE_LONG = 10000;
  public static readonly LIFE_RIDICULOUS = 20000;

  protected speed: Vec2d;
  protected lifeTime: number;
  protected y_pos: number;
  protected y_speed: number;
  protected facingLeft: boolean;
  protected damage: number;
  protected shadowOffset: number;

  constructor(scene: GameScene, x: number, y: number, z:number, texture: string, name: string, speed: Vec2d, damage: number, shadowOffset: number, lifeTime: number, anim?: string)
  {
    super(scene, x, y, texture, name, anim, true);

    this.y_pos = z;
    //add ourself to the projectile list
    GameScene.sceneProjectiles.push(this);
    this.lifeTime = lifeTime;
    this.speed = speed;
  }

  public tick(cursors = null, delta = null)
  {
    super.tick(cursors, delta);

    if(this.lifeTime <= 0) {
      //remove yourself from the update list and kill yourself so you're not in the scene
      this.destroy();
    } else {
      this.lifeTime -= delta;
    }
  }

  public destroy()
  {
    const id = GameScene.sceneProjectiles.indexOf(this);
    GameScene.sceneProjectiles.splice(id, 1);
    super.destroy();
  }
}

export default Projectile;
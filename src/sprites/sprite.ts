import 'phaser';
import Vec2d from '../math/vec2d';
import { GameScene } from '../scenes/gamescene';

class Sprite 
{
  protected static readonly TOP: number = -2500;
  protected sprite: Phaser.GameObjects.Sprite;
  protected scene: GameScene;
  protected dropshadow: Phaser.GameObjects.Sprite;
  protected position: Vec2d;
  protected displayOffset: Vec2d;
  protected name: string;

  constructor(scene: GameScene, x: number, y: number, texture: string, name: string, anim?:string, dropshadow: boolean = false)
  {
    this.scene = scene;

    if(dropshadow) {
      this.dropshadow = scene.add.sprite(x, y, 'dropshadow');
      this.dropshadow.setScale(0.6);
      this.dropshadow.alpha = 0.35;
    }

    if(anim) {
      this.sprite = scene.add.sprite(x, y, texture, anim);
    } else {
      this.sprite = scene.add.sprite(x, y, texture);
    }

    this.position = new Vec2d(x, y);
    
    this.sprite.setPosition(this.position.x, this.position.y);
    this.displayOffset = Vec2d.ZERO();
    this.name = name;
  }

  public tick(cursors = null, delta = null)
  {
    this.sprite.setPosition(this.position.x - this.displayOffset.x, this.position.y - this.displayOffset.y);
  }

  protected calculateAnimationState()
  {
  }

  protected createAnimations(scene: GameScene)
  {

  }

  public destroy()
  {
    this.sprite.destroy();
    if(this.dropshadow) {
      this.dropshadow.destroy();
    }
  }

  protected createAnim(scene: GameScene, animName: string, 
    start: number, end: number, prefix: string, fr:number, repeaet: number, key: string) 
  {
    var frameNames = scene.anims.generateFrameNames(animName, {
      start: start,
      end: end,
      zeroPad: 3,
      prefix: prefix,
      suffix: '.png'
    });
    
    scene.anims.create({
      key: key,
      frames: frameNames,
      frameRate: fr,
      repeat: repeaet
    });
    
  }

  public getSprite()
  {
    return this.sprite;
  }

  public setPosition(pos: Vec2d) {
    this.position = pos;
  }

  public getPosition(): Vec2d {
    return this.position;
  }

  public setOffset(val: Vec2d) {
    this.displayOffset = val;
  }

  public getName() : string
  {
    return this.name;
  }

  public getScene() : GameScene
  {
    return this.scene;
  }
}

export default Sprite;
import 'phaser';
import Vec2d from '../math/vec2d';
import MoveableSprite from '../sprites/moveablesprite';
import { GameScene } from '../scenes/gamescene';
import PhysicSprite from './physicsprite';
import Attack from '../powerups/attack';

class ActionSprite extends MoveableSprite
{
  public static readonly WEAK = 1;
  public static readonly MEDIUM = 2;
  public static readonly STRONG = 3;
  public static readonly JUICED = 4;

  protected dropshadow: Phaser.GameObjects.Sprite;
  protected walkCollider: Vec2d;
  protected y_pos: number;
  protected y_speed: number;
  protected speed: Vec2d;
  protected displayOffset: Vec2d;
  protected grounded: boolean;
  protected attacking: boolean;
  protected attackOffset: Vec2d;
  protected facingLeft: boolean;
  protected held: boolean;
  protected attackTimer: number;
  protected holding: PhysicSprite;
  protected holdOffset: Vec2d;
  protected strength: number;
  protected battleStance: boolean;
  protected battleTimer: number;

  protected attacks: Array<Attack>;
  protected latestAttack: Attack;

  constructor(scene: GameScene, x: number, y: number, texture: string, name: string, anim?:string, dropshadow: boolean = true)
  {
    super(scene, x, y, texture, name, anim, dropshadow);

    this.walkCollider = new Vec2d(this.sprite.displayWidth, 20);
    this.y_pos = y;
    this.sprite.setPosition(this.position.x, this.y_pos);
    this.dropshadow.setPosition(this.position.x, this.position.y);
    this.grounded = true;
    this.speed = Vec2d.ZERO();
    this.y_speed = 0;
    this.sprite.setOrigin(0.5, 1);
    this.attacking = false;
    this.held = false;
    this.attackTimer = 0;
    this.holding = null;
    this.holdOffset = Vec2d.ZERO();
    this.battleStance = false;
    this.battleTimer = 0;

    GameScene.sceneSprites.push(this);
  }

  public destroy()
  {
    const id = GameScene.sceneSprites.indexOf(this);
    GameScene.sceneSprites.splice(id, 1);
    super.destroy();
  }

  public tick(cursors = null, delta: number = null)
  {
    if(this.held !== true) {
      this.y_pos += this.y_speed;
      this.y_speed += GameScene.GRAVITY;
      this.position = this.position.plus(this.speed);

      if (this.position.y - this.walkCollider.y < GameScene.GAMETOP) {
        this.position.y = GameScene.GAMETOP + this.walkCollider.y;
        if(this.grounded) {
          this.speed.y = 0; //set this for the background scrolling
        }
      }
      if (this.position.y > 562) {
        this.position.y = 562;
        this.speed.y = 0; //set this for the background scrolling
      }
      if (this.y_pos > this.position.y && this.y_speed > 0) {
        this.grounded = true;
      }
      if (this.grounded === true) {
        this.y_speed = 0;
        this.y_pos = this.position.y;
      }

      if (this.attackTimer > 0) {
        this.attackTimer -= delta;
      }

      this.sprite.setPosition(this.position.x, this.y_pos);
      this.updateLeft();
      //shift the y with the y pos so i don't jump forever
      this.dropshadow.setPosition(this.position.x, this.position.y);
      this.calculateAnimationState();
      this.updateHolding();
      this.updateStanceTimer(delta);
      this.updateFacingDirection();
    }
  }

  /***
   * default implimentation override this in the more base classes to 
   * change how it works ( or if you're feeling fancy you can reset this)
   * function to something else
   */
  public hitCollider() : Vec2d
  {
    return new Vec2d(this.sprite.displayWidth, this.sprite.displayHeight);
  }

  public getSpeed(): Vec2d 
  {
    return this.speed;
  }

  public getPosition() : Vec2d
  {
    return new Vec2d(this.sprite.x, this.sprite.y);
  }

  public getZ()
  {
    return this.y_pos;
  }

  public getHeldPosition() : Vec2d
  {
    return new Vec2d(this.position.x + this.holdOffset.x, this.y_pos + this.holdOffset.y);
  }

  public setHeldPosition(position: Vec2d) : void
  {
    this.sprite.setPosition(position.x, position.y);
  }

  public setStunned(val: boolean) : void 
  {
    this.held = val;
  }

  public pickup(target: PhysicSprite) : void
  {
    this.holding = target;
  }

  public drop() : void
  {
    this.holding = null;
  }

  public setY_Pos(val: number)
  {
    this.y_pos = val;
  }

  public addY_speed(val: number)
  {
    this.y_speed += val;
  }

  public setSpeed(val: Vec2d) : void
  {
    this.speed = val;
  }

  public addSpeed(val: Vec2d) : void
  {
    this.speed = this.speed.plus(val);
  }

  public setAttackTime(val: number)
  {
    this.attackTimer = val;
  }

  public setupThrow()
  {
    this.holding.setPosition(this.position);
    this.holding.setY_Pos(this.y_pos);
  }

  public getThrowOffset() : number
  {
    return this.holdOffset.y;
  }

  public updateHolding()
  {
    //move the held object into the homlding position
    if(this.holding !== null) {
      this.holding.setPosition(new Vec2d(this.position.x + this.holdOffset.x, this.position.y + this.holdOffset.y));
      this.holding.setY_Pos(this.y_pos + this.holdOffset.y);
    }
  }

  public getStrength() : number
  {
    return this.strength;
  }

  protected updateSpriteLocation()
  {
    this.sprite.setPosition(this.position.x, this.y_pos);
    this.dropshadow.setPosition(this.position.x, this.position.y);
    this.calculateAnimationState();
  }

  public lockDown(lockTime: number) : void
  {
    this.attackTimer = lockTime;
  }

  protected forceInScreen()
  {
    if (this.position.y - this.walkCollider.y < GameScene.GAMETOP) {
      this.position.y = GameScene.GAMETOP + this.walkCollider.y;

      if(this.grounded) {
        this.speed.y = 0; // stop it from falling anymore
      }
    }

    if (this.position.y > 562) {
      this.position.y = 562;
      this.speed.y = 0;
    }

    if (this.y_pos > this.position.y && this.y_speed > 0) {
      this.grounded = true; //this is when we hit the ground
    }

    if (this.grounded === true) {
      this.y_speed = 0;
      this.y_pos = this.position.y;
    }
  }

  protected updateFacingDirection()
  {
    if(this.isLeft())
    {
      this.sprite.scaleX = -1;
    }
    else 
    {
      this.sprite.scaleX = 1;
    }
  }

  public isLeft() {
    return this.facingLeft; 
  }

  public setAttacking() {
    this.attacking = true;
  }

  protected updateStanceTimer(delta: number)
  {
    if(this.battleTimer > 0) {
      this.battleTimer -= delta;
    } else {
      this.battleStance = false;
    }
  }

  protected setStance()
  {
    this.battleTimer = 5000;
    this.battleStance = true;
  }
}

export default ActionSprite;
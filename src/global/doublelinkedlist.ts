import Node from './node';
import { idText } from 'typescript';

class DoubleLinkedList
{
  head: Node;
  tail: Node;
  count: number;

  constructor()
  {
    this.head = null;
    this.tail = null;
    this.count = 0;
  }

  public addNode(newNode: Node) : void
  {
    if(!this.head) {
      this.head = newNode;
      this.tail = newNode;
    } else {
      this.tail.next = newNode;
      this.tail = newNode;
    }
    this.count++;
  }

  public addNodeAfter(newNode: Node, findNode: Node) : void
  {
    const node = this.findNode(findNode);

    if(node) {
      if(node.next === null) {
        //great just push it in
        node.next = newNode;
      } else {
        newNode.next = node.next;
        node.next = newNode;
      }
    }

    this.count++;
  }

  public findNode(findNode: Node) : Node
  {
    let iter = this.head;

    while(iter) {
      if(iter === findNode) {
        return iter;
      }
    }

    return null;
  }

  public removeNode(findNode: Node) : void
  {
    const node = this.findNode(findNode);

    if(node) {
      if(node === this.tail) {
        node.prev = null;
        this.tail = node.prev;
      }
    }
    this.count--;
  }

  public removeNodeAtIndex(index: number) : void
  {
    let i = 0;
    let iter = this.head;

    if(index > this.count) {
      return;
    }

    while(i < index) {
      iter = iter.next;
    }

    if(iter) {
      if(iter === this.tail) {
        //its the end just move the tail back
        this.tail = iter.prev;
        this.tail.next = null;
      }
    }

    this.count --;
  }

  public getNodeIndex(findNode: Node) : number
  {
    let iter = this.head;
    let index = 0;

    while(iter) {
      if(iter === findNode) {
        break;
      }
      index++;
    }

    return index;
  }
}

export default DoubleLinkedList;
class Node
{
  public data: any;
  public next: Node;
  public prev: Node;

  constructor(val: any)
  {
    this.data = val;
    this.next = null;
    this.prev = null;
  }
}

export default Node;
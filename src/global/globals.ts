import Vec2d from '../math/vec2d';

class Globals 
{
  public static mapPos: Vec2d = Vec2d.ZERO();
}

export default Globals;
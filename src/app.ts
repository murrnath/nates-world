import "phaser";
import { GameScene } from "./scenes/gamescene";

const config: Phaser.Types.Core.GameConfig = {
  title: "Nates World",
  width: 1000,
  height: 562,
  parent: "game",
  scene: [GameScene],
  physics: {
    default: "none",
  },
  backgroundColor: "#000000"
};

export class NatesWorldGame extends Phaser.Game {
  constructor(config: Phaser.Types.Core.GameConfig) {
    super(config);
  }
}

window.onload = () => {
  var game = new NatesWorldGame(config);
};
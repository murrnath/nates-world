import GrappleAttack from '../powerups/grappleattack';
import Vec2d from '../math/vec2d';
import ActionSprite from '../sprites/actionsprite';

class PickUpObject extends GrappleAttack
{
  constructor(name: string, anim: string, owner: ActionSprite, offset: Vec2d)
  {
    super(name, anim, new Vec2d(50, 50), offset , 50, 255, 100, owner, 10);
  }

  public attack() 
  {
    //do all of the base clas stuff to try and actaully grab something
    if(!this.target) {
      super.attack();
    } else {
      //we want to throw this target 
      const throwSpeed = new Vec2d(0, -12);

      if(this.owner.isLeft()) {
        throwSpeed.x = -20;
      } else {
        throwSpeed.x = 20;
      }

      this.owner.setupThrow();
      this.target.thrown(throwSpeed, this.owner.getThrowOffset());
      this.target.setHeld(false);
      this.target = null;
      this.owner.drop();
    }

    this.owner.setAttackTime(this.lockTime);
  }
}

export default PickUpObject;
import RangedAttack from '../powerups/rangedattack';
import Vec2d from '../math/vec2d';
import ActionSprite from '../sprites/actionsprite';
import Purse from '../sprites/projectiles/purse';
import { GameScene } from '../scenes/gamescene';

class ThrowPurse extends RangedAttack
{
  constructor(name: string, anim: string, owner: ActionSprite, offset: Vec2d)
  {
    super(name, anim, new Vec2d(50, 10), offset, 10, 500, 10, owner);
  }

  public attack()
  {
    const throwPosition = this.owner.getPosition().plus(this.offset);
    let purse;

    if(this.owner.isLeft()) {
      purse = new Purse(this.owner.getScene(), throwPosition.x, this.owner.getPosition().y, throwPosition.y, 10, new Vec2d(10, 0), this.owner);
    } else {
      purse = new Purse(this.owner.getScene(), throwPosition.x, this.owner.getPosition().y, throwPosition.y, 10, new Vec2d(-10, 0), this.owner);
    }
    
    super.attack();
  }
}

export default ThrowPurse;
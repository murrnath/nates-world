import MeleeAttack from '../powerups/meleeattack';
import Vec2d from '../math/vec2d';
import ActionSprite from '../sprites/actionsprite';

class Punch extends MeleeAttack
{
  constructor(name: string, anim: string, owner: ActionSprite, offset:Vec2d)
  {
    super(name, anim, new Vec2d(50, 10), offset, 10, 255, 10, owner);
  }

  public attack()
  {
    super.attack();
  }
}

export default Punch;